# Listing of the SEP-All-Cear-API sep-all-clear.conf file lines

There are a number of lines that should just stay the way they are as they depend upon the implementation repository
for the SEP-All-Cear-API process and the configuration in the Dockerfile.  However, there are some that should change
when deployed by somone other than the [Data Mining Lab](http://dmlab.cs.gsu.edu/) at [Georgia State University](http://www.gsu.edu/). 

Below are those lines:

***

### ServerAdmin

	ServerAdmin dkempton1@gsu.edu
	
This is the email address of the server admin that is given when errors are encountered by the Apache Web Server process.
    
### ServerName
    
    ServerName dmlab.cs.gsu.edu
    
This is the name of the server that the Apache Web Server process resides.
    
### ServerAlias
  
    ServerAlias www.dmlab.cs.gsu.edu
    
This is an alias that the server can also be reached at. More than one can be provided.

### ErrorLog

	ErrorLog "|/usr/bin/rotatelogs /app/log/sep_all_clear_api_error.log.%Y-%m-%d-%H_%M 5M"
	
This is the error log for the Apache Web Server process. It is configured to roll over each time the log file reaches
5 Megabytes. The name and size of the file should really be the only things that are changed, as the Dockerfile and 
deployment docker-compose expects the log files to be in this location within the container.

    CustomLog "|/usr/bin/rotatelogs -l /app/log/sep_all_clear_api_access.log.%Y.%m.%d 86400" combined
    
This is is the access log for the Apache Web Server process. It is configured to roll over each day. The name and frequency
the file rolls over should be the only things that are changed, as the Dockerfile and deplyment docker-compose expects the
log files to be in this location within the container.

***

[Return to README](../README.md)

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](../LICENSE.txt)  

![alt text](../images/gplv3-88x31.png)

***

© 2022 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](http://dmlab.cs.gsu.edu/)

[Georgia State University](http://www.gsu.edu/)

