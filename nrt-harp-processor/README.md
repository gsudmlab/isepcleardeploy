# NRT-HARP-Processor

* [TEST BUILD](#markdown-header-test-build)
* [CONFIGURATION](#markdown-header-configuration)

***

## TEST Build

This build section only for building and running an individual container using the local Dockerfile image.

* If you want to run without a container see the repository for
  the [NRT-HARP-Processor](https://bitbucket.org/gsudmlab/nrt-harp-data-processing) process.
* If deploying the entire system, go to the main deployment process utilizing docker-compose and the docker-compose.yml.
  See the main [README.md](../README.md) file.

To build:
 
	docker build --rm -t nrt-harp-processor:1.1 .
 
From the directory containing the Dockerfile and all the other files required for build.
 
To run with current config:

First create a Network for both containers to communicate on.

	docker network create sep

Then run mysql:

	docker run --network sep --name sep-mysql-box -e MYSQL_ROOT_PASSWORD=root123 -e MYSQL_DATABASE=sep_pred -p 3306:3306 -d mysql:latest --default-authentication-plugin=mysql_native_password
	
If you are already using port 3306 you need to change the first 3306, this is the port on the host that you are mapping port 3306 of the container to. This will be the port you can point your MySQL admin tool to in order to look at the tables/data from your desktop. The rest should be setup already.

Then run the container

	docker run -dit -v /data/log:/app/log -v /data/SHARPS/nrt:/app/data --network sep --name=nrt-harp-processor-service nrt-harp-processor:1.1
	
Here the -v /data/log:... is where you want to place your log files from the processor.  Change the /data/log to where ever on your host machine you want it.

***

## CONFIGURATION

The default config file for this process is config.ini. It is copied into the image at build time.

A full listing of the lines lines in the config file and what they control are listed in the [CONFIG.md](CONFIG.md) file.

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](../LICENSE.txt)  

![alt text](../images/gplv3-88x31.png)

***

© 2022 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](http://dmlab.cs.gsu.edu/)

[Georgia State University](http://www.gsu.edu/)
