# NRT-ISEP-Clear Deployment


## TOC
* [CONFIG](#markdown-header-config)
    * [SEP-All-Clear-API](#markdown-header-nrt-cme-speed-predictor)
    * [NRT-All-Clear-Aggregator](#markdown-header-nrt-cme-speed-predictor)
    * [NRT-CME-Speed-Predictor](#markdown-header-nrt-cme-speed-predictor)
    * [NRT-CME-Flare-Processor](#markdown-header-nrt-cme-flare-processor)
    * [NRT-Eruption-Predictor](#markdown-header-nrt-eruption-predictor)
    * [NRT-Flare-Predictor](#markdown-header-nrt-flare-predictor)
    * [NRT-HARP-Processor](#markdown-header-nrt-harp-processor)
* [CLONE](#markdown-header-clone)
* [RUN](#markdown-header-run)
* [License](./LICENSE.txt)


***

There are a number of processes invloved in this system. Below you will find links to the various config file documentation files. 

***
***
## CONFIG
***
### SEP-All-Clear-API

This process provides the [RESTful API](https://restfulapi.net/) that allows access to the all clear predictions in a json format.


* The config file documenation can be found [here](./sep-all-clear-api/CONFIG.md).

* The config file can be found [here](./sep-all-clear-api/config.ini).

* The config file documenation for the Apache Web Service can be found [here](./sep-all-clear-api/SEP-ALL-CLEAR.md).

* The config file for the Apache Web Service can be found [here](./sep-all-clear-api/sep-all-clear.conf).

* The repository for the project can be found [here](https://bitbucket.org/gsudmlab/sep-all-clear-api).

***
### NRT-All-Clear-Aggregator

This process aggregates all the different flare, eruptive flare, and CME speed predictions for the active regions currently present at the time of the process run. This produces an overall all-clear probability estimate for the next 24 hours from the time of process run.

* The config file documenation can be found [here](./nrt-all-clear-aggregator/CONFIG.md).

* The config file can be found [here](./nrt-all-clear-aggregator/config.ini).

* The repository for the project can be found [here](https://bitbucket.org/gsudmlab/nrt-sep-all-clear-aggregator).


***
### NRT-CME-Speed-Predictor

This process produces a prediction about the speed of a CME an acitve region is likely to produce assuming an eruptive flare occures within the next 24 hours.

* The config file documenation can be found [here](./nrt-cme-speed-predictor/CONFIG.md).

* The config file can be found [here](./nrt-cme-speed-predictor/config.ini).

* The repository for the project can be found [here](https://bitbucket.org/gsudmlab/nrt-cme-speed-predictor).


***

### NRT-CME-Flare-Processor

This process downloads various pieces of information about recently reported CMEs, Flares, and the Proton/Electron flux information recorded by GOES.

* The config file documenation can be found [here](./nrt-cme-flare-processor/CONFIG.md).

* The config file can be found [here](./nrt-cme-flare-processor/config.ini).

* The repository for the project can be found [here](https://bitbucket.org/gsudmlab/nrt-cme-flare-processing).


***

### NRT-Eruption-Predictor

This process produces a prediction about whether an acitve region is likely to produce an eruptive flare within the next 24 hours.

* The config file documenation can be found [here](./nrt-eruption-predictor/CONFIG.md).

* The config file can be found [here](./nrt-eruption-predictor/config.ini).

* The repository for the project can be found [here](https://bitbucket.org/gsudmlab/nrt-eruption-predictor).


***

### NRT-Flare-Predictor

This process produces a prediction about whether an acitve region is likely to produce a flare within the next 24 hours.

* The config file documenation can be found [here](./nrt-flare-predictor/CONFIG.md).

* The config file can be found [here](./nrt-flare-predictor/config.ini).

* The repository for the project can be found [here](https://bitbucket.org/gsudmlab/nrt-flare-predictor).


***

### NRT-HARP-Processor

This process produces downloads and processes the HMI HARP information to produce the parameters needed by the various prediction processes.

* The config file documenation can be found [here](./nrt-harp-processor/CONFIG.md).

* The config file can be found [here](./nrt-harp-processor/config.ini).

* The repository for the project can be found [here](https://bitbucket.org/gsudmlab/nrt-harp-data-processing).

***
## CLONE
Assuming that the prerequisites git, docker, and docker-compose are installed, the repository needs to be cloned to your local machine.

### Step 1: Go Home ###
Navigate to your home (~) directory (or where you want to downloade the ropository to).

	$ cd ~

### Step 2: Make a place to store repo ###
	
	$ mkdir repos

Or whatever you want to name it. This is just a folder to hold your git repository, it won't be the final folder of the repository.

	$ cd ~/repos

Go to the place we will store the repository.

### Step 3: Clone the repo ###
Clone the repository to your local machine with the following command. 

	git clone --branch V0.1.1 --single-branch https://bitbucket.org/gsudmlab/isepcleardeploy.git

You should then see a directory in your repos directory that contains the deployment repository. Navigate into that directory

	cd ./isepcleardeploy

Next will be to run/build the images.

***
## RUN

	docker-compose up --build -d

From the direcotry containing the docker-compose.yml file. This will build the images and deploy them as services on your
machine. 

## Stop

	docker-compose down
	
From the direcotry containing the docker-compose.yml file.

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant Awards: No. AC1443061 and AC1931555. 

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)  

 ![alt text](./images/gplv3-88x31.png)

***

© 2022 Dustin Kempton, Berkay Aydin, Rafal Angryk
 
[Data Mining Lab](http://dmlab.cs.gsu.edu/)
 
[Georgia State University](http://www.gsu.edu/)